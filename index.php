<?php

class Menu extends RecursiveIteratorIterator
{
    public function getHtml(Menu $menu): string
    {
        $result = '';

        foreach ($menu as $item) {
            if (empty($item['title'])) {
                continue;
            }

            $result .= sprintf('<span>%s</span></br>', str_repeat('-', $this->getDepth()) . $item['title']);
        }

        return $result;
    }
}

$lines = file('data.txt');
$data = [];

foreach ($lines as $line) {
    list($nodeId, $parentId, $nodeName) = explode('|', $line);
    $data[$nodeId] = ['title' => $nodeName, 'parent' => $parentId];
}

$menu = [];

foreach ($data as $index => &$node) {
    if (!$node['parent']) {
        $menu[$index] = &$node;
        continue;
    }

    $data[$node['parent']][$index] = &$node;
}

$menu = new Menu(new RecursiveArrayIterator($menu), RecursiveIteratorIterator::SELF_FIRST);

echo $menu->getHtml($menu);
